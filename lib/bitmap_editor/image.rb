# frozen_string_literal: true

module BitmapEditor
  class Image
    class OutOfRange < StandardError
      def initialize(msg)
        super(msg)
      end
    end

    BASE_COLOR = 'O'
    SHIFT = 1

    attr_accessor :table

    def create_table(width, height)
      self.table = Array.new(height) { Array.new(width) { BASE_COLOR } }
    end

    def clear_table
      create_table(current_width, current_height)
    end

    def color(row, col, color)
      y, x = normalize_coordinates(row, col)
      validate_coordinates(y, x)
      table[y][x] = color
    end

    def formatted_output
      Helpers::Formatter.format(table)
    end

    def print_content
      puts formatted_output
    end

    private

    def current_height
      table.size
    end

    def current_width
      table.first&.size || 0
    end

    def normalize_coordinates(y, x)
      [normalize_coordinate(y), normalize_coordinate(x)]
    end

    def normalize_coordinate(coord)
      # Bitmaps starts at coordinates 1,1. (From task description)
      result = coord - SHIFT
      result.negative? ? coord : result
    end

    def validate_coordinates(y, x)
      if current_height <= y || current_width <= x
        raise OutOfRange, "Can not execute command. "\
          "Coordinates (#{y}, #{x}) not in (#{current_height}, #{current_width})"
      end
    end
  end
end
