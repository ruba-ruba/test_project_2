# frozen_string_literal: true

module BitmapEditor
  class Builder
    attr_reader :image

    def initialize(image = Image.new)
      @image = image
    end

    def run(file_path)
      File.foreach(file_path).each do |line|
        parsed_line = line_parser.parse(line)
        command = command_matcher.match(parsed_line)
        command.validate
        command.apply(image)
      end
    end

    private

    def line_parser
      BitmapEditor::LineParser
    end

    def command_matcher
      BitmapEditor::CommandMatcher
    end
  end
end
