# frozen_string_literal: true

module BitmapEditor
  class CommandMatcher
    class NotFound < StandardError
      def initialize(msg)
        super(msg)
      end
    end

    def self.match(line)
      new(line).match
    end

    def initialize(line)
      @line = line
    end

    def match
      klass = commands.find { |command_class| command_class::IDENTIFIER == line.command }
      unless klass
        msg = "command for identifier #{line.command} not found"
        raise BitmapEditor::CommandMatcher::NotFound, msg
      end
      klass.new(line.args)
    end

    private

    attr_reader :line

    def commands
      BitmapEditor::Commands.constants.map do |c|
        klass = BitmapEditor::Commands.const_get(c)
        next unless klass.is_a?(Class)
        klass
      end.compact
    end
  end
end
