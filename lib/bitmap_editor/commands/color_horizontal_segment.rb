# frozen_string_literal: true

module BitmapEditor
  module Commands
    class ColorHorizontalSegment < Base
      IDENTIFIER = 'H'

      COOMAND_ARGS = Struct.new(:col1, :col2, :row, :color) do
        include ::BitmapEditor::Commands::ArgsValidator

        def initialize(*)
          super
          self.col1 = col1.to_i
          self.col2 = col2.to_i
          self.row = row.to_i
        end
      end

      def apply(image)
        (command_args.col1..command_args.col2).each do |col|
          image.color(command_args.row, col, command_args.color)
        end
      end

      def command_args
        @command_args ||= COOMAND_ARGS.new(*args)
      end
    end
  end
end
