# frozen_string_literal: true

module BitmapEditor
  module Commands
    class CreateTable < Base
      IDENTIFIER = 'I'

      COOMAND_ARGS = Struct.new(:width, :height) do
        include ::BitmapEditor::Commands::ArgsValidator

        def initialize(*)
          super
          self.width = width.to_i
          self.height = height.to_i
        end
      end

      def command_args
        @command_args ||= COOMAND_ARGS.new(*args)
      end
    end
  end
end
