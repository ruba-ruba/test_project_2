# frozen_string_literal: true

module BitmapEditor
  module Commands
    class PrintContent < Base
      IDENTIFIER = 'S'

      private

      def valid?
        true
      end
    end
  end
end
