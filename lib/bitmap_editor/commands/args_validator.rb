# frozen_string_literal: true

module BitmapEditor
  module Commands
    module ArgsValidator
      def included(base)
        base.extend(self)
      end

      def valid?
        map.to_a.all?
      end
    end
  end
end
