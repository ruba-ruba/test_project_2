# frozen_string_literal: true

module BitmapEditor
  module Commands
    class ColorVerticalSegment < Base
      IDENTIFIER = 'V'

      COOMAND_ARGS = Struct.new(:column, :row1, :row2, :color) do
        include ::BitmapEditor::Commands::ArgsValidator

        def initialize(*)
          super
          self.column = column.to_i
          self.row1 = row1.to_i
          self.row2 = row2.to_i
        end
      end

      def apply(image)
        (command_args.row1..command_args.row2).each do |row|
          image.color(row, command_args.column, command_args.color)
        end
      end

      def command_args
        @command_args ||= COOMAND_ARGS.new(*args)
      end
    end
  end
end
