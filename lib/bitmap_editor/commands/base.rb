# frozen_string_literal: true

require 'securerandom'
require 'active_support/all'

module BitmapEditor
  module Commands
    class Base
      class NotValid < StandardError
        def initialize(msg)
          super(msg)
        end
      end

      IDENTIFIER = SecureRandom.uuid

      attr_reader :args

      def initialize(args = [])
        @args = args
      end

      def apply(image)
        image.send command, *command_args
      end

      def validate
        unless valid?
          msg = "arguments (#{args&.join(' ')}) are not sufficient for command #{self.class.to_s.demodulize}"
          raise NotValid, msg
        end
      end

      private

      delegate :valid?, to: :command_args

      def command
        self.class.to_s.demodulize.underscore
      end

      def command_args
        []
      end
    end
  end
end
