# frozen_string_literal: true

module BitmapEditor
  module Commands
    class Color < Base
      IDENTIFIER = 'L'

      COOMAND_ARGS = Struct.new(:x, :y, :color) do
        include ::BitmapEditor::Commands::ArgsValidator

        def initialize(*)
          super
          self.x = x.to_i
          self.y = y.to_i
        end
      end

      def apply(image)
        image.color(command_args.y, command_args.x, command_args.color)
      end

      def command_args
        @command_args ||= COOMAND_ARGS.new(*args)
      end
    end
  end
end
