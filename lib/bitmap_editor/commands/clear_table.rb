# frozen_string_literal: true

module BitmapEditor
  module Commands
    class ClearTable < Base
      IDENTIFIER = 'C'

      private

      def valid?
        true
      end
    end
  end
end
