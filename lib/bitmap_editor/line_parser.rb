# frozen_string_literal: true

module BitmapEditor
  class LineParser
    LINE = Struct.new(:command, :args) do
      def initialize(command, args = {})
        super
      end
    end
    SEPARATOR = / /

    def self.parse(line)
      new(line).parse
    end

    def initialize(line)
      @line = line&.strip
    end

    def parse
      command, *args = line.split(SEPARATOR)
      LINE.new(command, args)
    end

    private

    attr_reader :line
  end
end
