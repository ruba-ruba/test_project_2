# frozen_string_literal: true

module BitmapEditor
  module Helpers
    class Formatter
      ROW_SEPARATOR = ''
      LINE_SEPARATOR = "\n"

      class << self
        def format(table)
          Array(table).map { |row| row.join(ROW_SEPARATOR) }.join(LINE_SEPARATOR)
        end
      end
    end
  end
end
