# frozen_string_literal: true

require 'spec_helper'

RSpec.describe BitmapEditor::Builder do
  let(:file) { File.open(file_path) }
  let(:builder) { described_class.new }

  describe '#run' do
    subject { builder.run(file) }

    context 'with valid file' do
      let(:file_path) { 'spec/support/fixtures/example1.txt' }

      let(:expected_result) do
        <<~RESULT
          OOOOO
          OOZZZ
          AWOOO
          OWOOO
          OWOOO
          OWOOO
        RESULT
      end

      before { subject }

      it 'builds correct image' do
        expect(builder.image.formatted_output).to eq expected_result.strip
      end
    end

    context 'when result cleared' do
      let(:file_path) { 'spec/support/fixtures/clear_table.txt' }

      let(:expected_result) do
        <<~RESULT
          OOOOO
          OOOOO
          OOOOO
          OOOOO
          OOOOO
          OOOOO
        RESULT
      end

      before { subject }

      it 'builds correct image' do
        expect(builder.image.formatted_output).to eq expected_result.strip
      end
    end

    context 'with invalid command' do
      let(:file_path) { 'spec/support/fixtures/invalid_command.txt' }

      it 'raises NotFound exception' do
        expect { subject }.to raise_error BitmapEditor::CommandMatcher::NotFound
      end
    end

    context 'with missing arguments' do
      let(:file_path) { 'spec/support/fixtures/invalid_command_args.txt' }

      it 'raises NotValid exception' do
        expect { subject }.to raise_error BitmapEditor::Commands::Base::NotValid
      end
    end

    context 'with blank line' do
      let(:file_path) { 'spec/support/fixtures/with_empty_line.txt' }

      it 'raises NotFound exception' do
        expect { subject }.to raise_error BitmapEditor::CommandMatcher::NotFound
      end
    end

    context 'when coordinates out of range' do
      let(:file_path) { 'spec/support/fixtures/out_of_range_coords.txt' }

      it 'raises NotFound exception' do
        expect { subject }.to raise_error BitmapEditor::Image::OutOfRange
      end
    end
  end
end
