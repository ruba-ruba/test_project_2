# frozen_string_literal: true

require 'spec_helper'

RSpec.describe BitmapEditor::LineParser do
  describe '.parse' do
    subject { described_class.parse(line) }

    shared_examples :parsing_line_into_command do
      it 'parses line into result object' do
        expect(subject).to eq parsed_line
      end
    end

    context 'line with arguments' do
      let(:width) { '5' }
      let(:height) { '6' }
      let(:line) { "I #{width} #{height}" }
      let(:parsed_line) do
        BitmapEditor::LineParser::LINE.new('I', [width, height])
      end

      it_behaves_like :parsing_line_into_command
    end

    context 'line without arguments' do
      let(:line) { 'C' }
      let(:parsed_line) do
        BitmapEditor::LineParser::LINE.new('C', [])
      end

      it_behaves_like :parsing_line_into_command
    end
  end
end
