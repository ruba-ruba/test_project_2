# frozen_string_literal: true

require 'spec_helper'

RSpec.describe BitmapEditor::Commands::ColorHorizontalSegment do
  let(:image) { double(:image).as_null_object }

  describe '#apply' do
    subject do
      described_class.new([3, 5, 2, 'Z']).apply(image)
    end

    it 'colors horizontal segment' do
      (3..5).each do |col|
        expect(image).to receive(:color).with(2, col, 'Z').once
      end
      subject
    end
  end
end
