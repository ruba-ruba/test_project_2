# frozen_string_literal: true

require 'spec_helper'

RSpec.describe BitmapEditor::Commands::Color do
  let(:image) { double(:image).as_null_object }

  describe '#apply' do
    subject do
      described_class.new([5, 6, 'W']).apply(image)
    end

    it 'add image' do
      expect(image).to receive(:color).with(6, 5, 'W').once
      subject
    end
  end
end
