# frozen_string_literal: true

require 'spec_helper'

RSpec.describe BitmapEditor::Commands::PrintContent do
  let(:image) { double(:image).as_null_object }

  describe '#apply' do
    subject do
      described_class.new.apply(image)
    end

    it 'show image' do
      expect(image).to receive(:print_content).once
      subject
    end
  end
end
