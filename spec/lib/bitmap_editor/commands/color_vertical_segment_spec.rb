# frozen_string_literal: true

require 'spec_helper'

RSpec.describe BitmapEditor::Commands::ColorVerticalSegment do
  let(:image) { double(:image).as_null_object }

  describe '#apply' do
    subject do
      described_class.new([2, 3, 6, 'W']).apply(image)
    end

    it 'colors vertical segment' do
      (3..6).each do |row|
        expect(image).to receive(:color).with(row, 2, 'W').once
      end
      subject
    end
  end
end
