# frozen_string_literal: true

require 'spec_helper'

RSpec.describe BitmapEditor::Commands::CreateTable do
  let(:image) { double(:image).as_null_object }

  describe '#apply' do
    subject do
      described_class.new([5, 6]).apply(image)
    end

    it 'creates table with base color' do
      expect(image).to receive(:create_table).with(5, 6).once
      subject
    end
  end
end
