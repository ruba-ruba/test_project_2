# frozen_string_literal: true

require 'spec_helper'

RSpec.describe BitmapEditor::CommandMatcher do
  describe '.match' do
    subject { described_class.match(parsed_line) }

    shared_examples :matching_line_to_command do
      it 'matches line to command class' do
        expect(subject.class).to eq expected_command.class
      end

      it 'has correct args' do
        expect(subject.args).to eq expected_command.args
      end
    end

    context 'create image command' do
      let(:args) { %w[5 6] }
      let(:parsed_line) { BitmapEditor::LineParser::LINE.new('I', args) }
      let(:expected_command) do
        BitmapEditor::Commands::CreateTable.new(args)
      end

      it_behaves_like :matching_line_to_command
    end

    context 'clear table command' do
      let(:parsed_line) { BitmapEditor::LineParser::LINE.new('C', []) }
      let(:expected_command) do
        BitmapEditor::Commands::ClearTable.new
      end

      it_behaves_like :matching_line_to_command
    end

    context 'add color command' do
      let(:x) { '1' }
      let(:y) { '3' }
      let(:color) { 'A' }
      let(:args) { [x, y, color] }
      let(:parsed_line) { BitmapEditor::LineParser::LINE.new('L', args) }
      let(:expected_command) do
        BitmapEditor::Commands::Color.new(args)
      end

      it_behaves_like :matching_line_to_command
    end

    context 'add vertical segment command' do
      let(:row_1) { '3' }
      let(:row_2) { '6' }
      let(:color) { 'W' }
      let(:column) { '2' }
      let(:args) { [column, row_1, row_2, color] }
      let(:parsed_line) { BitmapEditor::LineParser::LINE.new('V', args) }
      let(:expected_command) do
        BitmapEditor::Commands::ColorVerticalSegment.new(args)
      end

      it_behaves_like :matching_line_to_command
    end

    context 'add horizontal segment command' do
      let(:row) { '2' }
      let(:column_1) { '3' }
      let(:column_2) { '5' }
      let(:color) { 'Z' }
      let(:args) { [column_1, column_2, color, color] }
      let(:parsed_line) { BitmapEditor::LineParser::LINE.new('H', args) }
      let(:expected_command) do
        BitmapEditor::Commands::ColorHorizontalSegment.new(args)
      end

      it_behaves_like :matching_line_to_command
    end

    context 'print content command' do
      let(:parsed_line) { BitmapEditor::LineParser::LINE.new('S', []) }
      let(:expected_command) do
        BitmapEditor::Commands::PrintContent.new
      end

      it_behaves_like :matching_line_to_command
    end

    context 'when command not found' do
      let(:parsed_line) { BitmapEditor::LineParser::LINE.new('NON_COMMAND', []) }

      it 'raises not found error' do
        expect { subject }.to raise_error BitmapEditor::CommandMatcher::NotFound
      end
    end
  end
end
