# frozen_string_literal: true

require 'spec_helper'

RSpec.describe BitmapEditor::Image do
  let(:image) { described_class.new }

  describe '#create_table' do
    let(:expected_table) do
      Array.new(height) { Array.new(width) { base_color } }
    end

    let(:base_color) { 'O' }
    let(:height) { 6 }
    let(:width) { 5 }

    it 'creates epmty table colored with O' do
      expect { image.create_table(width, height) }
        .to change { image.table }
        .from(nil).to(expected_table)
    end
  end

  describe '#color' do
    let!(:base_table) { Array.new(5) { Array.new(5) { 'O' } } }
    let!(:expected_table) do
      new_table = Marshal.load(Marshal.dump(base_table))
      new_table[y - 1][x - 1] = color
      new_table
    end
    let(:color) { 'C' }
    let(:x) { 1 }
    let(:y) { 3 }

    before do
      image.table = Marshal.load(Marshal.dump(base_table))
    end

    it 'changes cell at x,y to new color' do
      expect { image.color(y, x, color) }
        .to change { image.table[y - 1] }
        .from(base_table[y]).to(expected_table[y - 1])
    end
  end

  describe '#print_content' do
    let!(:base_table) { Array.new(5) { Array.new(5) { 'O' } } }
    let(:expected_output) do
      <<~EOF
        OOOOO
        OOOOO
        OOOOO
        OOOOO
        OOOOO
      EOF
    end

    before do
      image.table = base_table
    end

    it 'formats image content' do
      expect(STDOUT).to receive(:puts).with(expected_output.strip).once
      image.print_content
    end
  end

  describe '#clear_table' do
    let!(:base_table) { Array.new(5) { Array.new(5) { 'X' } } }
    let!(:expected_table) { Array.new(5) { Array.new(5) { 'O' } } }

    before do
      image.table = base_table
    end

    it 'changes all cells to base color' do
      expect { image.clear_table }
        .to change { image.table }
        .from(base_table).to(expected_table)
    end
  end
end
